module PolkuRest
  class GetAll < AppService
    attr_reader :token, :url

    def initialize(token, url, params={})
      @token = token
      @url = url
      @params = params
      @data = nil
    end

    def call
      uri = URI(@url)
      request = Net::HTTP::Get.new(uri)
      request["Content-Type"] = "application/json"
      request["Authorization"] = "Bearer #{@token}"
      request["X-API-KEY"] = @token
      request.body = @params.to_json
      response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
        http.request(request)
      end
      @data = response.body
      self
    rescue StandardError => e
      @data = {error: e.message}.to_json
      self
    end

    def data_as_hash
      JSON.parse(@data)
    end

    def data_as_object
      JSON.parse(@data, object_class: OpenStruct)
    end

    def data_as_raw
      @data
    end
  end
end