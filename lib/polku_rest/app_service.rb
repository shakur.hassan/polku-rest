module PolkuRest
  class AppService
    def self.call(*args, &block)
      new(*args, &block).call
    rescue ArgumentError => e
      "#{e.message}"
    end
  end
end