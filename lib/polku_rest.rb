# frozen_string_literal: true

require_relative "polku_rest/version"
require 'uri'
require 'net/http'
require 'json'

module PolkuRest
  autoload :AppService, "polku_rest/app_service"
  autoload :Error, "polku_rest/error"
  autoload :GetAll, "polku_rest/get_all"
  autoload :GetOne, "polku_rest/get_one"
  autoload :Create, "polku_rest/create"
  autoload :Update, "polku_rest/update"
  autoload :Delete, "polku_rest/delete"
end
