# PolkuRest

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/polku_rest`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem "polku_rest", git: "https://gitlab.com/shakur.hassan/polku-rest.git", branch: 'main'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install polku_rest

## Usage

```ruby
# Get Request
token = "Bearer some_token"
url = "https://example.com/api/v1/users"
# Data as Hash
response_as_hash = PolkuRest::GetAll.call(token, url, params).data_as_hash
# Get id of first element
response_as_hash[0]['id']

# Data as Object
response_as_object = PolkuRest::GetAll.call(token, url, params).data_as_object
# Get id of first element
response_as_object[0].id

# Other requests
PolkuRest::GetOne.call(token, url, id).data_as_raw
PolkuRest::Update.call(token, url, id, params).data_as_raw
PolkuRest::Create.call(token, url, params).data_as_raw
PolkuRest::Delete.call(token, url, id).data_as_raw

# Note: Calling only call method will return the whole response object
# Use one of data_as_hash, data_as_object, data_as_raw methods to get the data
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/polku_rest.
